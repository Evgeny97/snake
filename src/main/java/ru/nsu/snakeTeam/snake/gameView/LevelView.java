package ru.nsu.snakeTeam.snake.gameView;

import ru.nsu.snakeTeam.snake.gameModel.BodyPart;
import ru.nsu.snakeTeam.snake.gameModel.Direction;
import ru.nsu.snakeTeam.snake.gameModel.Level;
import ru.nsu.snakeTeam.snake.gameModel.LevelPosition;
import ru.nsu.snakeTeam.snake.math.Matrix4f;

import java.util.ArrayList;

import static org.lwjgl.glfw.GLFW.*;

public class LevelView implements GameObjectView {
    static Matrix4f pr_matrix;
    private Background background;


    private ArrayList<GameObjectView> snakeBody = new ArrayList<>();

    private Level level = new Level();
    private final float width;

    private final float height;

    public LevelView() {
        width = level.getWidth() * BodyPartView.getSize();
        height = level.getHeight() * BodyPartView.getSize();
        pr_matrix = Matrix4f.orthographic(-width / 2.0f, width / 2.0f,
                -height / 2.0f, height / 2.0f, -1.0f, 1.0f);


    }

    public void initGraphics() {
        background = new Background(width, height);
    }

    public void updateModel(int lastDirectionButton) {
        Direction direction = setDirection(lastDirectionButton);
        level.move(direction);
        updateBodyView();
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getLevelWidth() {
        return level.getWidth();
    }

    public float getLevelHeight() {
        return level.getHeight();
    }

    private void updateBodyView() {
        ArrayList<BodyPart> bodyParts = new ArrayList<>(level.getBodyParts());

        snakeBody.clear();
        BodyType type;
        Direction direction = null;
        Direction lastDirection;

        for (int i = 0; i < bodyParts.size(); i++) {
            BodyPart bodyPart = bodyParts.get(i);
            lastDirection = direction;
            direction = bodyPart.getDirection();
            if (i == 0) {
                type = BodyType.HEAD;
            } else if (i == bodyParts.size() - 1) {
                type = BodyType.TAIL;
            } else if (direction == lastDirection) {
                type = BodyType.STRAIGHT;
            } else {
                type = BodyType.CORNER;
            }
            snakeBody.add(new BodyPartView(type, direction, lastDirection,
                    levelToViewTransform(bodyPart.getPosition())));
        }
    }

    private ViewPosition levelToViewTransform(LevelPosition levelPosition) {
        ViewPosition viewPosition = new ViewPosition();
        viewPosition.x = (levelPosition.x - (level.getWidth() / 2.0f) + 0.5f) * BodyPartView.getSize();
        viewPosition.y = (levelPosition.y - (level.getHeight() / 2.0f) + 0.5f) * BodyPartView.getSize();
        return viewPosition;
    }

    private Direction setDirection(int lastDirectionButton) {
        Direction direction;
        switch (lastDirectionButton) {
            case GLFW_KEY_DOWN:
                direction = Direction.DOWN;
                break;
            case GLFW_KEY_LEFT:
                direction = Direction.LEFT;
                break;
            case GLFW_KEY_RIGHT:
                direction = Direction.RIGHT;
                break;
            default:
                direction = Direction.UP;
                break;
        }
        return direction;
    }

    @Override
    public void render() {
        background.render();
        for (GameObjectView bodyPart : snakeBody) {
            bodyPart.render();
        }
    }
}
