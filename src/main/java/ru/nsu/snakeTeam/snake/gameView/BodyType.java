package ru.nsu.snakeTeam.snake.gameView;

public enum BodyType {
    HEAD, STRAIGHT, CORNER, TAIL
}
