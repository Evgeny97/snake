package ru.nsu.snakeTeam.snake.gameView;

public class ViewPosition {
    float x;
    float y;

    public ViewPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public ViewPosition() {
    }
}
