package ru.nsu.snakeTeam.snake.gameView;

import ru.nsu.snakeTeam.snake.graphic.Shader;
import ru.nsu.snakeTeam.snake.graphic.Texture;
import ru.nsu.snakeTeam.snake.graphic.VertexArray;
import ru.nsu.snakeTeam.snake.visual.Driver;

public class Background implements GameObjectView {

    private static float[] vertices;

    private static byte[] indices = new byte[]{
            0, 1, 2,
            0, 2, 3};

    private static float[] textureCoords;
    private VertexArray vao;

    private static Texture texture = new Texture("textures/grass.jpg");

    private static Shader shader = new Shader("shaders/background.vert", "shaders/background.frag");

    public Background(float width, float height) {
        vertices = new float[]{
                -width / 2.0f, height / 2.0f, -0.5f,
                -width / 2.0f, -height / 2.0f, -0.5f,
                width / 2.0f, -height / 2.0f, -0.5f,
                width / 2.0f, height / 2.0f, -0.5f};

        textureCoords = new float[]{
                0.0f, height *0.05f,
                0.0f, 0.0f,
                width * 0.05f, 0.0f,
                width * 0.05f, height *0.05f};

        vao = new VertexArray(vertices, indices, textureCoords);
        shader.enable();
        shader.setUniformMat4f("pr_matrix", LevelView.pr_matrix);
        shader.disable();
    }

    public void render() {
        texture.bind();
        shader.enable();
        vao.draw();
        shader.disable();
        texture.unbind();
    }

}
