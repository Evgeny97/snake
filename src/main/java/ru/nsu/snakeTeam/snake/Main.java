package ru.nsu.snakeTeam.snake;

import ru.nsu.snakeTeam.snake.visual.Driver;

public class Main {
    public static void main(String[] args) {
        Driver driver = new Driver();
        driver.start();
    }
}
