package ru.nsu.snakeTeam.snake.input;

import org.lwjgl.glfw.GLFWKeyCallback;

import static org.lwjgl.glfw.GLFW.*;


public class KeyboardImport extends GLFWKeyCallback {
    private boolean[] keys = new boolean[1000];

    public int getLastDirectionButton() {
        return lastDirectionButton;
    }

    private int lastDirectionButton;

    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) {
//        keys[key] = action != GLFW_RELEASE;
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_UP) {
                lastDirectionButton = GLFW_KEY_UP;
            } else if (key == GLFW_KEY_DOWN) {
                lastDirectionButton = GLFW_KEY_DOWN;
            } else if (key == GLFW_KEY_LEFT) {
                lastDirectionButton = GLFW_KEY_LEFT;
            } else if (key == GLFW_KEY_RIGHT) {
                lastDirectionButton = GLFW_KEY_RIGHT;
            }
        }
    }

    public boolean isKeyDown(int keycode) {
        return keys[keycode];
    }
}
