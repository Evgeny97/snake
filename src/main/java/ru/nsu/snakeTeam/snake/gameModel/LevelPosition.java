package ru.nsu.snakeTeam.snake.gameModel;

public class LevelPosition {
    public int x;
    public int y;

    public LevelPosition() {
        x = 0;
        y = 0;
    }

    public LevelPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public LevelPosition clone(){
        return new LevelPosition(x,y);
    }
}
