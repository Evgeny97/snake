package ru.nsu.snakeTeam.snake.gameModel;

import java.util.ArrayDeque;

public class Level {
    ArrayDeque<BodyPart> bodyParts = new ArrayDeque<>();
    //ArrayDeque<BodyPart> bodyParts = new ArrayDeque<>();

    Direction movementDirection = Direction.UP;
    boolean isAte = false;
    private final static int step = 1;
    private final int width = 5;
    private final int height = 5;


    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Level() {
        bodyParts.add(new BodyPart(0, 0, Direction.UP));
        bodyParts.add(new BodyPart(0, -1, Direction.UP));
        bodyParts.add(new BodyPart(0, -2, Direction.UP));
        bodyParts.add(new BodyPart(0, -3, Direction.UP));
    }

    public ArrayDeque<BodyPart> getBodyParts() {
        return bodyParts;
    }

    public void move(Direction newDirection) {
        if (!newDirection.isOpposite(movementDirection)) {
            movementDirection = newDirection;
        }

        BodyPart lastHead = bodyParts.getFirst();
        LevelPosition newPosition = lastHead.position.clone();
        switch (movementDirection) {
            case UP:
                newPosition.y += step;
                break;
            case DOWN:
                newPosition.y -= step;
                break;
            case RIGHT:
                newPosition.x += step;
                break;
            case LEFT:
                newPosition.x -= step;
                break;

        }
        bodyParts.addFirst(new BodyPart(newPosition, movementDirection));
        if (!isAte) {
            bodyParts.removeLast();
        }
    }

//    public void changeMovementDirection(Direction direction) {
//        movementDirection = direction;
//    }

    public void eatFood() {
        isAte = true;
    }
}
