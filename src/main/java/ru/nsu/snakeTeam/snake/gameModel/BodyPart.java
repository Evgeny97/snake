package ru.nsu.snakeTeam.snake.gameModel;

public class BodyPart {
    public Direction getDirection() {
        return direction;
    }

    public LevelPosition getPosition() {
        return position;
    }

    Direction direction;
    LevelPosition position;

    public BodyPart(LevelPosition position, Direction direction) {
        this.direction = direction;
        this.position = position;
    }

    public BodyPart(int x, int y, Direction direction) {
        this.direction = direction;
        this.position = new LevelPosition(x, y);
    }
}
