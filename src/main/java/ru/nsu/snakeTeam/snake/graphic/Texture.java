package ru.nsu.snakeTeam.snake.graphic;

import org.lwjgl.BufferUtils;
import ru.nsu.snakeTeam.snake.utils.Utilities;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;

import static org.lwjgl.opengl.GL30.*;

public class Texture {
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    private int width, height;
    private int texture;

    public Texture(String path) {
        texture = load(path);
    }

    private int load(String path) {
        int[] pixels = null;
        try {
            BufferedImage image = ImageIO.read(new FileInputStream(path));
            width = image.getWidth();
            height = image.getHeight();
            pixels = new int[width * height];
            image.getRGB(0, 0, width, height, pixels, 0, width);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int[] data = new int[width * height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int a = (pixels[x + y * width] & 0xff000000) >> 24;
                int r = (pixels[x + y * width] & 0xff0000) >> 16;
                int g = (pixels[x + y * width] & 0xff00) >> 8;
                int b = (pixels[x + y * width] & 0xff);

                data[x + (height - y - 1) * width] = a << 24 | b << 16 | g << 8 | r;
            }
        }


//        for (int i = 0; i < width * height; i++) {
//            int a = (pixels[i] & 0xff000000) >> 24;
//            int r = (pixels[i] & 0xff0000) >> 16;
//            int g = (pixels[i] & 0xff00) >> 8;
//            int b = (pixels[i] & 0xff);
//
//            data[i] = a << 24 | b << 16 | g << 8 | r;
//        }

        int result = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, result);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        //float borderColor[] = { 1.0f, 1.0f, 0.0f, 1.0f };
        //glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, Utilities.createFloatBuffer(borderColor));
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                Utilities.createIntBuffer(data));
        glBindTexture(GL_TEXTURE_2D, 0);
        return result;
    }

    public void bind() {
        glBindTexture(GL_TEXTURE_2D, texture);
    }

    public void unbind() {
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
