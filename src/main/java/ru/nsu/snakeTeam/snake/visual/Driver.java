package ru.nsu.snakeTeam.snake.visual;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import ru.nsu.snakeTeam.snake.gameView.LevelView;
import ru.nsu.snakeTeam.snake.input.*;
import ru.nsu.snakeTeam.snake.math.Matrix4f;


import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.system.MemoryUtil.NULL;


public class Driver {
    private GLFWErrorCallback errorCallback;

    private long window;
    private boolean running = true;

    private KeyboardImport keyCallback;
    private MouseInput mouseInput;

    private int width = 800;
    private int height = 800;
    private float scale = 6;


    LevelView levelView;
    private double testTimeAccum = 0;


    private void init() {
        errorCallback = GLFWErrorCallback.createPrint(System.err);
        glfwSetErrorCallback(errorCallback);

        if (!glfwInit()) {
            System.err.println("init error");
        }

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        levelView = new LevelView();
        width = (int) (levelView.getWidth() * scale);
        height = (int) (levelView.getHeight() * scale);

        window = glfwCreateWindow(width, height, "Snake", NULL, NULL);


        if (window == NULL) {
            System.err.println("window create error");
        }

        keyCallback = new KeyboardImport();
        glfwSetKeyCallback(window, keyCallback);

        mouseInput = new MouseInput();
        glfwSetCursorPosCallback(window, mouseInput);

        GLFWVidMode videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

        glfwSetWindowPos(window, (videoMode.width() - width) / 2, (videoMode.height() - height) / 2);

        glfwMakeContextCurrent(window);

        GL.createCapabilities();

        glfwShowWindow(window);

        glfwSwapInterval(0);


        System.out.println(glGetString(GL_VERSION));

        glClearColor(0.0f, 0.4f, 0.4f, 1.0f);
        glEnable(GL_DEPTH_TEST);

        levelView.initGraphics();
    }

    private void render() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        levelView.render();
        glfwSwapBuffers(window);
    }

    private void update() {

        glfwPollEvents();
        long lastTime = System.nanoTime();
        levelView.updateModel(keyCallback.getLastDirectionButton());
        testTimeAccum += System.nanoTime() - lastTime;
//        if (keyCallback.isKeyDown(GLFW_KEY_SPACE)) {
//            System.out.println("space");
//        }
    }

    public void start() {
        init();

        long lastTime = System.nanoTime();
        double delta = 0.0;
        double ns = 1_000_000_000.0 / 3;
        long timer = System.currentTimeMillis();
        int updates = 0;
        int frames = 0;
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            if (delta >= 1.0) {
                update();
                updates++;
                delta--;

            }
            render();

            frames++;
            if (System.currentTimeMillis() - timer > 1000) {
                System.out.println(System.currentTimeMillis() - timer + " / " + testTimeAccum/1_000_000.0);
                testTimeAccum = 0;
                timer += 1000;
                System.out.println(updates + " ups, " + frames + " fps");
                updates = 0;
                frames = 0;

            }

            if (glfwWindowShouldClose(window)) {
                running = false;
            }
        }
        glfwDestroyWindow(window);
        glfwTerminate();
    }
}
