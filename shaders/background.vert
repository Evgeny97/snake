#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 tc;

out vec2 textureCoords;

uniform mat4 pr_matrix;

void main()
{
    gl_Position = pr_matrix * vec4(position,1.0f);
    textureCoords = tc;
}