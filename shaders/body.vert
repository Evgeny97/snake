#version 330 core

layout (location = 0) in vec4 vertexCoords;
layout (location = 1) in vec2 tc;

out vec2 textureCoords;

uniform mat4 view_matrix;
uniform mat4 model_matrix;

void main()
{
   // gl_Position = pr_matrix *  (vec4(position,1.0f) + vec4(vertexCoords,1.0f));
    gl_Position = view_matrix * model_matrix * vertexCoords;
    textureCoords = tc;
}